package com.example.petshop.net.repository

import com.example.petshop.net.data.AnimalType
import com.example.petshop.net.data.Brand
import com.example.petshop.net.data.RawGood
import com.example.petshop.net.data.Type
import com.example.petshop.net.data.response.*
import com.example.petshop.net.retrofit.RetrofitClient
import com.example.petshop.other.*
import com.example.petshop.viewmodel.GoodsViewModel
import com.google.gson.Gson
import io.reactivex.Flowable

class GoodsRepository {

    val goods = ArrayList<RawGood>()
    val brands = ArrayList<Brand>()
    val animalTypes = ArrayList<AnimalType>()
    val types = ArrayList<Type>()

    fun getGoods(): Flowable<GoodsResponse> {
        return RetrofitClient.retrofit.getGoods(GoodsViewModel.categoryId).map {
            goods.addAll(it.goods)
            return@map it
        }
    }

    fun getGoodById(id: Int) = RetrofitClient.retrofit.getGoodById(id).map {
        val response = it.string()
        val good = Gson().fromJson<GoodsResponse>(response, GoodsResponse::class.java)
        when (good.goods[0].categoryId) {
            FOOD -> return@map Gson().fromJson<FoodResponse>(response, FoodResponse::class.java).goods
            CARE -> return@map Gson().fromJson<CareResponse>(response, CareResponse::class.java).goods
            CAGE -> return@map Gson().fromJson<CageResponse>(response, CageResponse::class.java).goods
            TOYS -> return@map Gson().fromJson<ToysResponse>(response, ToysResponse::class.java).goods
            FILLERS -> return@map Gson().fromJson<GoodsResponse>(response, GoodsResponse::class.java).goods
            ANIMALS -> return@map Gson().fromJson<AnimalResponse>(response, AnimalResponse::class.java).goods
        }
        return@map good.goods
    }

    fun getBrands() : Flowable<BrandResponse>{
        return RetrofitClient.retrofit.getBrands(GoodsViewModel.categoryId).map {
            it.brands.add(0, Brand())
            brands.addAll(it.brands)
            return@map it
        }
    }

    fun getAnimalTypes() : Flowable<AnimalTypeResponse>{
        return RetrofitClient.retrofit.getAnimalTypes(GoodsViewModel.categoryId).map {
            it.types.add(0, AnimalType())
            animalTypes.addAll(it.types)
            return@map it
        }
    }

    fun getTypes() : Flowable<TypeResponse>{
        return RetrofitClient.retrofit.getTypes(GoodsViewModel.categoryId).map{
            it.types.add(0,Type())
            types.addAll(it.types)
            return@map it
        }
    }
}