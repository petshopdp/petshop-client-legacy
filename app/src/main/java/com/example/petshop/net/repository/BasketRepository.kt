package com.example.petshop.net.repository

import com.example.petshop.net.data.Basket
import com.example.petshop.net.data.RawGood
import com.example.petshop.net.data.response.BasketResponse
import com.example.petshop.net.data.response.MessageResponse
import com.example.petshop.net.retrofit.RetrofitClient
import io.reactivex.Flowable

class BasketRepository {
    val RESPONSE_GOOD = 1
    val RESPONS_BAD = 2
    var basket = ArrayList<Basket>()

    fun load(userId: Int) : Flowable<BasketResponse>{
        return RetrofitClient.retrofit.getBasket(userId).map { //Я этот код написал с целью решения проблемы
            if (basket.isEmpty()) {                            //рассинхронизации корзины и мне интересно то,
                basket.addAll(it.baskets)                       //насколько правильно оно это делает
            } else
                if (basket != it.baskets) {
                    basket = it.baskets
                }
            return@map it
        }
    }

    fun uploadGoodToBasket(userId: Int, good: RawGood, count: Int = 1) : Flowable<MessageResponse> {
        var isExist = false
        for (i in basket) {
            if (i.good == good) {
                i.count += count
                isExist = true
                break
            }
        }
        return RetrofitClient.retrofit.uploadGoodToBasket(userId, good.id, count).map {
            if (it.message.id == RESPONSE_GOOD && !isExist)
                basket.add(Basket(good, count))
            return@map it
        }
    }

    fun deleteFromBasket(userId: Int, good: RawGood) : Flowable<MessageResponse> {
        return RetrofitClient.retrofit.deleteFromBasket(userId, good.id)
    }
}