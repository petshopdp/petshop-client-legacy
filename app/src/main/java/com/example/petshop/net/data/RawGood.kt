package com.example.petshop.net.data

import com.google.gson.annotations.SerializedName

open class RawGood() {
   var id = 0
   @SerializedName("category_id")
   var categoryId = 0
   var name = ""
   var brand = Brand()
   @SerializedName("animal_type")
   open var animalType = AnimalType()
   var price = 0
   var count = 0
   var type = Type()
   var url = ""
   var description = ""
   constructor(id: Int, name: String, brand: Brand, animalType: AnimalType, price: Int, count: Int, type: Type, url: String, description: String): this() {
       this.id = id
       this.name = name
       this.brand = brand
       this.price = price
       this.animalType = animalType
       this.count = count
       this.type = type
       this.url = url
       this.description = description
   }
}