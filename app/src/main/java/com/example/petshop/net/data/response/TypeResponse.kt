package com.example.petshop.net.data.response

import com.example.petshop.net.data.Type

class TypeResponse{

    val types = ArrayList<Type>()
}