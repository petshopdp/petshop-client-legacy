package com.example.petshop.net.repository

import com.example.petshop.net.data.response.MessageResponse
import com.example.petshop.net.data.response.UserResponse
import com.example.petshop.net.retrofit.RetrofitClient
import io.reactivex.Flowable

class UserRepository {

    fun login(username: String?, password: String?, token: String) : Flowable<UserResponse> {
        return RetrofitClient.retrofit.login(username, password, token)
    }

    fun signup(username: String, password: String, name: String, surname: String, email: String) : Flowable<MessageResponse> {
        return RetrofitClient.retrofit.signUp(username, password, name, surname, email)
    }

    fun logout(token: String, userId: Int) : Flowable<MessageResponse> {
        return RetrofitClient.retrofit.logout(token, userId)
    }

    fun changePassword(id: Int, oldPassword: String, newPassword: String) : Flowable<MessageResponse> {
        return RetrofitClient.retrofit.changePassword(id, oldPassword, newPassword)
    }

    fun changeUserData(id: Int, email: String, name: String, surname: String): Flowable<MessageResponse> {
        return RetrofitClient.retrofit.changeUserInfo(id, email, name, surname)
    }
}