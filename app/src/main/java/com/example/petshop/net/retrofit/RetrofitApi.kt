package com.example.petshop.net.retrofit

import com.example.petshop.net.data.response.*
import io.reactivex.Flowable
import okhttp3.ResponseBody
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface RetrofitApi {

    // for goods

    @GET("categories")
    fun getCategories() : Flowable<CategoryResponse>

    @GET("animal_types")
    fun getAnimalTypes(@Query("category_id") categoryId: Int) : Flowable<AnimalTypeResponse>

    @GET("types")
    fun getTypes(@Query("category_id") categoryId: Int) : Flowable<TypeResponse>

    @GET("brands")
    fun getBrands(@Query("category_id") categoryId: Int): Flowable<BrandResponse>

    @GET("goods")
    fun getGoods(@Query("category_id") categoryId: Int): Flowable<GoodsResponse>

    @GET("goods")
    fun getGoodById(@Query("id") id: Int) : Flowable<ResponseBody>

    //user

    @POST("signup")
    fun signUp(@Query("login")username: String, @Query("password") password: String,
               @Query("first_name") name: String, @Query("last_name") surname: String,
               @Query("email")email: String): Flowable<MessageResponse>

    @POST("users")
    fun changePassword(@Query("id") id: Int, @Query("old_password") oldPassword: String,
                   @Query("new_password") newPassword: String): Flowable<MessageResponse>

    @POST("users")
    fun changeUserInfo(@Query("id") id: Int, @Query("email") email: String,
                       @Query("first_name") name: String, @Query("last_name") surname: String): Flowable<MessageResponse>

    @POST("login")
    fun login(@Query("login")  username: String? = null, @Query("password") password: String? = null,
              @Query("device_tag") token: String): Flowable<UserResponse>

    @POST("logout")
    fun logout(@Query("device_tag") token: String, @Query("user_id")userId: Int) : Flowable<MessageResponse>

    //baskets

    @GET("baskets")
    fun getBasket(@Query("user_id") id: Int) : Flowable<BasketResponse>

    @POST("baskets")
    fun uploadGoodToBasket(@Query("user_id")userId: Int, @Query("good_id") goodId: Int,
                           @Query("count") count: Int) : Flowable<MessageResponse>

    @DELETE("baskets")
    fun deleteFromBasket(@Query("user_id")userId: Int, @Query("good_id") goodId: Int) : Flowable<MessageResponse>

}