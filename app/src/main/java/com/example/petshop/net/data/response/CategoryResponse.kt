package com.example.petshop.net.data.response

import com.example.petshop.net.data.Category

class CategoryResponse {

    var categories = ArrayList<Category>()
}