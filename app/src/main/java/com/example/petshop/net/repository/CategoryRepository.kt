package com.example.petshop.net.repository

import com.example.petshop.net.data.Category
import com.example.petshop.net.data.response.CategoryResponse
import com.example.petshop.net.retrofit.RetrofitClient
import io.reactivex.Flowable

class CategoryRepository() {

    var categories = ArrayList<Category>()

    fun getCategories(): Flowable<CategoryResponse> {
        return RetrofitClient.retrofit.getCategories()
    }
}