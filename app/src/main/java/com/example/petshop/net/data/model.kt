package com.example.petshop.net.data

import android.graphics.drawable.Drawable
import com.google.gson.annotations.SerializedName

/*
* Это было сделано, потому что я точно не знаю можно ли так делать или нет,
* но я всё же решил, что можно. Принимаю всю ответственность на себя
*
 */

data class Message(var id: Int = 0, var text: String = "")

data class Help(var image: Drawable, var text: String = "")

data class Category(var id: Int = 0, var name: String = "", var url: String = "") {
    override fun toString(): String {
        return name
    }
}

data class Type(var id: Int = 0, var name: String = "") {
    override fun toString(): String {
        return name
    }
}

data class AnimalType(var id: Int = 0, var name: String = "") {
    override fun toString(): String {
        return name
    }
}

data class Brand(var id: Int = 0, var name: String = "") {
    override fun toString(): String {
        return name
    }
}

data class Breed(var id: Int = 0, var name: String = "") {
    override fun toString(): String {
        return name
    }
}

data class Basket(var good: RawGood, @SerializedName("count_in_basket")var count: Int = 0)

data class Food(var weight: Int = 0) : RawGood()

data class Toy(var somevar: Int = 0) : RawGood()

data class Cage(var length: Int = 0, var width: Int = 0, var height: Int = 0) : RawGood()

data class Animal(var breed: Breed, var sex: Char = 'M', var age: Int = 0) : RawGood()

data class Care(var somevar: Int = 0) : RawGood()
