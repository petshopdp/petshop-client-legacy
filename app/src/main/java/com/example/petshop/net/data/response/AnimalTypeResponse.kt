package com.example.petshop.net.data.response

import com.example.petshop.net.data.AnimalType
import com.google.gson.annotations.SerializedName

class AnimalTypeResponse{

    @SerializedName("animal_types")
    val types = ArrayList<AnimalType>()
}