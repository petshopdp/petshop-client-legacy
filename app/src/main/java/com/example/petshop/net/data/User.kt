package com.example.petshop.net.data

import com.google.gson.annotations.SerializedName

class User {
    var id: Int = 0
    var name: String = ""
    var surname: String = ""
    @SerializedName("username")
    var login: String = ""
    var password: String = ""
    var email: String = ""

    companion object {
        var activeUser = User()
    }
}