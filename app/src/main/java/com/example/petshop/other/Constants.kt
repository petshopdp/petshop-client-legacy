package com.example.petshop.other

const val FOOD = 1
const val CARE = 2
const val CAGE = 3
const val TOYS = 4
const val FILLERS = 5
const val ANIMALS = 6

val validation = Regex("(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#\$%^&*]{6,}")

val LINEAR = "linear"
val GRID = "grid"