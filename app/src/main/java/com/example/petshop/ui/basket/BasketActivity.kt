package com.example.petshop.ui.basket

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.BasketViewModel

class BasketActivity : BaseActivity() {

    private lateinit var binding: ViewDataBinding
    private lateinit var vm: BasketViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProviders.of(this).get(BasketViewModel::class.java)
        vm.create(this)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_basket)
        binding.setVariable(BR.vm, vm)
        initUI()
        vm.load()
    }


}
