package com.example.petshop.ui.category

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.CategoryViewModel


class CategoryActivity : BaseActivity() {

    lateinit var binding: ViewDataBinding
    lateinit var vm: CategoryViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_category)
        vm = ViewModelProviders.of(this).get(CategoryViewModel::class.java)
        vm.create()
        binding.setVariable(BR.vm, vm)
        initUI()
        vm.load()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }
}
