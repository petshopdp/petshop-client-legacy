package com.example.petshop.ui.help


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.Help


class HelpFragment() : Fragment() {
    lateinit var binding: ViewDataBinding
    lateinit var texts : Array<String>
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_help, container, false)
        texts = resources.getStringArray(R.array.help_txt)
        setHelp()
        return binding.root
    }

    fun setHelp() {
        var pos = arguments!!.getInt("POS")
        when (pos) {
            0 -> binding.setVariable(BR.help, Help(context!!.getDrawable(R.drawable.categories), texts[pos]))
            1 -> binding.setVariable(BR.help, Help(context!!.getDrawable(R.drawable.goods), texts[pos]))
            2 -> binding.setVariable(BR.help, Help(context!!.getDrawable(R.drawable.to_basket), texts[pos]))
            3 -> binding.setVariable(BR.help, Help(context!!.getDrawable(R.drawable.buy), texts[pos]))
            /*Насколько правильно я выдаю нужный фрагмент?
            Является ли то, что я сделал фабричным методом?
            Правильно ли то, что я для текста сделал массив, нужен ли он вообще?
            Нужно ли такой же организовать для картинок?
            */
        }
    }


    companion object {
        fun create(pos: Int) : HelpFragment {
            val bundle = Bundle()
            val fragment = HelpFragment()
            bundle.putInt("POS", pos)
            fragment.arguments = bundle
            return fragment
        }
    }
}
