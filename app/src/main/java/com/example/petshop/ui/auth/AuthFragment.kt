package com.example.petshop.ui.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.ui.base.BaseFragment
import com.example.petshop.viewmodel.AuthViewModel

class AuthFragment : BaseFragment() {

    lateinit var vm: AuthViewModel
    var binding: ViewDataBinding? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_auth, container, false)
        vm = (activity as AuthActivity).vm
        binding?.setVariable(BR.vm, vm)
        return binding?.root
    }

}
