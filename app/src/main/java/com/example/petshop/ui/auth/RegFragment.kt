package com.example.petshop.ui.auth


import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.viewmodel.AuthViewModel

class RegFragment : Fragment() {

    lateinit var binding: ViewDataBinding
    lateinit var vm: AuthViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reg, container, false)
        vm = (activity as AuthActivity).vm
        binding.setVariable(BR.vm, vm)
        binding.executePendingBindings()
        return binding.root
    }
}
