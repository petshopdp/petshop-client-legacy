package com.example.petshop.ui.goods

import android.os.Bundle
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.helper.AddFilterHelper
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.GoodsViewModel

class GoodsActivity : BaseActivity() {

    private lateinit var binding: ViewDataBinding
    private lateinit var vm: GoodsViewModel
    private lateinit var helper: AddFilterHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        vm = ViewModelProviders.of(this).get(GoodsViewModel::class.java)
        vm.create()
        GoodsViewModel.categoryId = intent.extras.getInt(CATEGORY_ID)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_goods)
        binding.setVariable(BR.vm, vm)
        initUI()
        addHelper()
        vm.load()
    }

    private fun addHelper() {
        val container = findViewById<FrameLayout>(R.id.container)
        helper = AddFilterHelper(container, vm)
        helper.initAppbar(intent.extras.getInt(CATEGORY_ID))
        vm.helper = helper
    }

    companion object {
        var CATEGORY_ID = "categoryId"
    }
}
