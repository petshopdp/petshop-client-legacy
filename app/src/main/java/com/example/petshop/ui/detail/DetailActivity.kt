package com.example.petshop.ui.detail

import android.os.Bundle
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.helper.AddDetailInfoHelper
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.DetailViewModel

class DetailActivity : BaseActivity() {

    lateinit var detailInfoHelper: AddDetailInfoHelper
    lateinit var binding: ViewDataBinding
    lateinit var linearLayout: LinearLayout
    lateinit var vm: DetailViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_detail)
        linearLayout = findViewById(R.id.linear_layout)
        detailInfoHelper = AddDetailInfoHelper(linearLayout)
        vm = ViewModelProviders.of(this).get(DetailViewModel::class.java)
        vm.create(this, detailInfoHelper)
        binding.setVariable(BR.vm, vm)
        
        initUI()
        vm.load(intent.extras.getInt(ITEM_ID))
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onStop() {
        super.onStop()
        finish()
    }

    companion object {
        val ITEM_ID = "itemId"
    }
}
