package com.example.petshop.ui.auth

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.R
import com.example.petshop.dagger.AppComponent
import com.example.petshop.dagger.AppModule
import com.example.petshop.dagger.DaggerAppComponent
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.AuthViewModel


class AuthActivity : BaseActivity() {
    lateinit var vm: AuthViewModel
    var binding: ViewDataBinding? = null
    var token = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_auth)
        appComponent = DaggerAppComponent.builder().appModule(AppModule()).build()
        token = getTokent()
        vm = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        vm.create(token,  getKey())
        initLD()
        liveData.value = DEFAULT
    }

    fun setFragment(type: Int) {
        var transaction = this.supportFragmentManager.beginTransaction()
            when (type) {
                HELLO_FRAGMENT -> transaction.replace(R.id.container, HelloFragment())
                REG_FRAGMENT -> transaction.replace(R.id.container, RegFragment()).addToBackStack(null)
                AUTH_FRAGMENT -> transaction.replace(R.id.container, AuthFragment()).addToBackStack(null)
            }
        transaction.commit()
    }
    fun getKey() : Int? {
        return if (intent.extras == null)
            null
        else
            intent.extras.getInt("logout")
    }

    private fun initLD() {
        liveData.value = DEFAULT
        liveData.observe(this, Observer{
            when(it) {
                DEFAULT -> {
                    setFragment(HELLO_FRAGMENT)
                }
                CHANGE_ACTIVITY -> {
                    startCategoryActivity(this)
                }

                HELLO_FRAGMENT -> {
                    onBackPressed()
                }

                REG_FRAGMENT -> {
                    setFragment(REG_FRAGMENT)
                }

                AUTH_FRAGMENT -> {
                    setFragment(AUTH_FRAGMENT)
                }
            }
            vm.clear()
        })
    }

    override fun onStop() {
        finish()
        super.onStop()
    }

    companion object {
        lateinit var appComponent: AppComponent
        val liveData = MutableLiveData<Int>()
        val DEFAULT = 0
        val CHANGE_ACTIVITY = 1
        val HELLO_FRAGMENT = 2
        val REG_FRAGMENT = 3
        val AUTH_FRAGMENT = 4
        val LOGOUT = 5
    }

}
