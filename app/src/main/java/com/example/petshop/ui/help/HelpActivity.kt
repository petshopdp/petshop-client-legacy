package com.example.petshop.ui.help

import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.example.petshop.R
import com.google.android.material.tabs.TabLayout

class HelpActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_help)
        var pagerAdapter = SectionPagerAdapter(supportFragmentManager, applicationContext)
        findViewById<ViewPager>(R.id.viewPager).adapter = pagerAdapter
        findViewById<TabLayout>(R.id.tablayout).setupWithViewPager(findViewById<ViewPager>(R.id.viewPager))
    }

    class SectionPagerAdapter(fm: FragmentManager, val context: Context) : FragmentPagerAdapter(fm) {

        override fun getItem(position: Int): Fragment {
            return HelpFragment.create(position)
        }


        override fun getCount() = 4
    }
}
