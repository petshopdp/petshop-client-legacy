package com.example.petshop.ui.user

import android.os.Bundle
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.ViewModelProviders
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.ui.base.BaseActivity
import com.example.petshop.viewmodel.UserViewModel

class UserActivity : BaseActivity() {

    lateinit var binding: ViewDataBinding
    lateinit var vm: UserViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user)
        vm = ViewModelProviders.of(this).get(UserViewModel::class.java)
        vm.create()
        binding.setVariable(BR.vm, vm)
        initUI()
    }

}
