package com.example.petshop.ui.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.provider.Settings
import android.view.LayoutInflater
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.drawerlayout.widget.DrawerLayout
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.User
import com.example.petshop.ui.auth.AuthActivity
import com.example.petshop.ui.basket.BasketActivity
import com.example.petshop.ui.category.CategoryActivity
import com.example.petshop.ui.help.HelpActivity
import com.example.petshop.ui.user.UserActivity
import com.google.android.material.navigation.NavigationView

abstract class BaseActivity() : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    fun startHelpActivity() {
        startActivity(Intent(this, HelpActivity::class.java))
    }

    fun startUserActivity() {
        startActivity(Intent(this, UserActivity::class.java))
    }

    fun startBasketActivity() {
        startActivity(Intent(this, BasketActivity::class.java))
    }

    fun startCategoryActivity() {
        startActivity(Intent(this, CategoryActivity::class.java))
    }
    fun startCategoryActivity(context: Context) {
        finish()
        startActivity(Intent(context, CategoryActivity::class.java))
    }

    fun startAuthActivity() {
        val intent = Intent(this, AuthActivity::class.java)
        intent.putExtra("logout", AuthActivity.LOGOUT)
        startActivity(intent)
    }

    fun getTokent() = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)

    fun initUI() {
        val drawer = findViewById<DrawerLayout>(R.id.drawer)
        val navigationView = findViewById<NavigationView>(R.id.navigation_view)
        val header = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(applicationContext), R.layout.header, navigationView, false)
        val toggle = ActionBarDrawerToggle(
            this, drawer, findViewById(R.id.toolbar), R.string.app_name, R.string.app_name
        )
        drawer.addDrawerListener(toggle)
        toggle.syncState()
        navigationView.setNavigationItemSelectedListener{
            when(it.itemId){
                R.id.to_basket -> {
                    startBasketActivity()
                    return@setNavigationItemSelectedListener true
                }
                R.id.to_category -> {
                    startCategoryActivity()
                    return@setNavigationItemSelectedListener true
                }
                R.id.to_help -> {
                    startHelpActivity()
                    return@setNavigationItemSelectedListener true
                }
                R.id.exit -> {
                    startAuthActivity()
                    return@setNavigationItemSelectedListener true
                }
            }
            return@setNavigationItemSelectedListener false
        }
        header.setVariable(BR.user, User.activeUser)
        navigationView.addHeaderView(header.root)
        navigationView.getHeaderView(0).setOnClickListener{startUserActivity()}

    }
    companion object {
    }

}