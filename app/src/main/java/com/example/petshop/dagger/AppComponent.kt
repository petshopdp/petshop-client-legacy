package com.example.petshop.dagger

import com.example.petshop.ui.category.CategoryActivity
import com.example.petshop.viewmodel.*
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class])
interface AppComponent {

    fun inject(categoryActivity: CategoryActivity)

    fun inject(categoryViewModel: CategoryViewModel)

    fun inject(goodsViewModel: GoodsViewModel)

    fun inject(basketViewModel: BasketViewModel)

    fun inject(detailViewModel: DetailViewModel)

    fun inject(authViewModel: AuthViewModel)

    fun inject(baseViewModel: BaseViewModel)

}