package com.example.petshop.dagger

import com.example.petshop.net.repository.BasketRepository
import com.example.petshop.net.repository.CategoryRepository
import com.example.petshop.net.repository.UserRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class AppModule() {

    private var categoryRep: CategoryRepository? = null
    private var basketRep: BasketRepository? = null
    private var userRep: UserRepository? = null

    @Provides
    @Singleton
    fun provideCategoryRepository() = CategoryRepository()

    @Provides
    @Singleton
    fun provideBasketRepository() = BasketRepository()

    @Provides
    @Singleton
    fun provideUserRepository() = UserRepository()
}