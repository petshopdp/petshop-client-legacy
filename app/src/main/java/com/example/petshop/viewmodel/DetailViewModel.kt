package com.example.petshop.viewmodel

import android.app.AlertDialog
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableInt
import androidx.databinding.ViewDataBinding
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.helper.AddDetailInfoHelper
import com.example.petshop.net.data.User
import com.example.petshop.net.repository.BasketRepository
import com.example.petshop.net.repository.GoodsRepository
import com.example.petshop.ui.auth.AuthActivity
import com.example.petshop.ui.base.BaseActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class DetailViewModel : BaseViewModel() {

    @Inject
    lateinit var basketRep: BasketRepository
    val rep = GoodsRepository()
    var dialogView: ViewDataBinding? = null
    var dialog: AlertDialog? = null
    var count: ObservableInt = ObservableInt(1)
    var helper: AddDetailInfoHelper? = null

    fun create(activity: BaseActivity, detailInfoHelper: AddDetailInfoHelper) {
        super.create()
        this.helper = detailInfoHelper
        AuthActivity.appComponent.inject(this)
        dialogView = DataBindingUtil.inflate(activity.layoutInflater, R.layout.dialog_add_good_to_basket,
            FrameLayout(activity), false)
        dialogView?.setVariable(BR.vm, this)
    }

    fun load(goodId: Int) {
        compositeDisposable.add(
            rep.getGoodById(goodId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    helper?.setItem(it[0])
                }
        )
    }
    fun openFragment(v: View) {
        dialog = AlertDialog.Builder(v.context)
            .setView(dialogView?.root)
            .show()
    }

    fun addGoodToBasket(v: View){
            compositeDisposable.add(
                basketRep.uploadGoodToBasket(User.activeUser.id, helper?.good!!, count.get())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe {
                        Snackbar.make(v, it.message.text, Snackbar.LENGTH_SHORT).show()
                    }
            )


    }
    fun closeFragment() {
        dialog?.dismiss()
    }

    fun incCount() = count.set(count.get()+1)

    fun decCount() {
        if (count.get() > 1)
            count.set(count.get()-1)
    }


}