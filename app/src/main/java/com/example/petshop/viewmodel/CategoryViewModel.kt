package com.example.petshop.viewmodel

import androidx.databinding.ObservableField
import com.example.petshop.adapters.rv.CategoryAdapter
import com.example.petshop.net.repository.CategoryRepository
import com.example.petshop.other.GRID
import com.example.petshop.ui.auth.AuthActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class CategoryViewModel : BaseViewModel() {

    @Inject
    lateinit var rep: CategoryRepository
    val adapter = ObservableField<CategoryAdapter>()

    override fun create() {
        super.create()
        AuthActivity.appComponent.inject(this)
    }

    fun load() {
        compositeDisposable.add(
            rep.getCategories()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe {
                adapter.set(CategoryAdapter(it.categories))
            })

    }



    companion object {
        @JvmStatic
        val MANAGER = GRID
    }
}