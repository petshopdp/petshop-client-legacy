package com.example.petshop.viewmodel

import android.app.AlertDialog
import android.view.View
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import androidx.databinding.ViewDataBinding
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.adapters.rv.BasketAdapter
import com.example.petshop.net.data.Basket
import com.example.petshop.net.data.User
import com.example.petshop.net.repository.BasketRepository
import com.example.petshop.other.LINEAR
import com.example.petshop.ui.auth.AuthActivity
import com.example.petshop.ui.base.BaseActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class BasketViewModel : BaseViewModel(){

    val count = ObservableInt(0)
    val price = ObservableInt(0)
    val adapter = ObservableField<BasketAdapter>()
    var dialog: AlertDialog? = null
    var dialogView: ViewDataBinding? = null
    @Inject lateinit var rep: BasketRepository

    fun create(activity: BaseActivity) {
        super.create()
        AuthActivity.appComponent.inject(this)
        dialogView = DataBindingUtil.inflate(activity.layoutInflater, R.layout.dialog_buy, FrameLayout(activity), false)
        dialogView?.setVariable(BR.vm, this)
    }

    fun load(){
        compositeDisposable.add(
            rep.load(User.activeUser.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    for (i in it.baskets) {
                        count.set(count.get()+i.count)
                        price.set(price.get()+i.good.price*i.count)
                    }
                    adapter.set(BasketAdapter(rep.basket, this))
                }
        )
    }

    fun openFragment(v: View) {
        dialog = AlertDialog.Builder(v.context)
            .setView(dialogView?.root)
            .show()

    }

    fun buy(v: View) {
        Snackbar.make(v, "Спасибо за покупку", Snackbar.LENGTH_SHORT).show()
    }

    fun closeFragment() {
        dialog?.dismiss()
    }

    fun removeItems(v: View, item: Basket, pos: Int) {

        compositeDisposable.add(
            rep.deleteFromBasket(User.activeUser.id, item.good)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.message.id == 0) {
                        count.set(count.get()-item.count)
                        price.set(price.get()-item.count * item.good.price)
                        rep.basket.remove(item)
                        adapter.get()?.refresh(pos)
                        Snackbar.make(v, it.message.text, Snackbar.LENGTH_SHORT).show()
                    }


                }
        )
    }
    companion object {
        @JvmStatic
        val MANAGER = LINEAR
    }
}