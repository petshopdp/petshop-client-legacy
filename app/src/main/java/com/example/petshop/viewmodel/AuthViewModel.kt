package com.example.petshop.viewmodel

import android.view.View
import com.example.petshop.net.data.User
import com.example.petshop.net.repository.UserRepository
import com.example.petshop.other.validation
import com.example.petshop.ui.auth.AuthActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.apache.commons.codec.digest.DigestUtils
import javax.inject.Inject

class AuthViewModel() : BaseViewModel() {

    @Inject
    lateinit var rep: UserRepository
    var token = ""
    var user = User()
    var username = ""
    var password = ""
    var repeat = ""
    var name = ""
    var surname = ""
    var email = ""

    fun create(token: String, key: Int?) {
        super.create()
        this.token = token
        AuthActivity.appComponent.inject(this)
        if (key != null)
                logout()
            else
                autoAuth()

    }

    fun onClickAuth() {
        AuthActivity.liveData.value = AuthActivity.AUTH_FRAGMENT
    }

    fun onClickReg() {
        AuthActivity.liveData.value = AuthActivity.REG_FRAGMENT
    }

    fun onClickBack() {
        AuthActivity.liveData.value = AuthActivity.HELLO_FRAGMENT
    }


    fun signUp(v: View) {
        if (password != repeat) Snackbar.make(v, "Пароли не совпадают!", Snackbar.LENGTH_SHORT).show()
        else
            if (!isValidPassword()) Snackbar.make(v, "Небезопасный пароль!", Snackbar.LENGTH_SHORT).show()
            else {
                compositeDisposable.add(
                    rep.signup(username, DigestUtils.md5Hex(password), name, surname, email)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe ({
                            Snackbar.make(v, "Вход есть", Snackbar.LENGTH_SHORT).show()

                        }, {
                            Snackbar.make(v, "Нет сети или низкая скорость", Snackbar.LENGTH_SHORT).show()
                        })
                )
            }

    }

    fun onSubmit(v: View) {
        compositeDisposable.add(
            rep.login(username, DigestUtils.md5Hex(password), token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                    {
                        if (it.users.id != 0){
                            user = it.users
                            User.activeUser = user
                            AuthActivity.liveData.value = AuthActivity.CHANGE_ACTIVITY
                        } else Snackbar.make(v, "Пользователь не существует!", Snackbar.LENGTH_SHORT).show()
                    },
                    {
                        Snackbar.make(v, "Нет сети или низкая скорость", Snackbar.LENGTH_SHORT).show()
                    }
                )
        )
    }

    fun clear() {
        username = ""
        password = ""
    }

    private fun autoAuth() {
        compositeDisposable.add(
            rep.login(null, null, token)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe (
                    {
                        if (it.users.id != 0){
                            user = it.users
                            User.activeUser = user
                            AuthActivity.liveData.value = AuthActivity.CHANGE_ACTIVITY
                        }
                    },
                    {

                    }
                )
        )
    }

    private fun logout() {
        compositeDisposable.add(
            userRep.logout(token, User.activeUser.id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe ()
        )
    }

    private fun isValidPassword() = validation.containsMatchIn(password)

}