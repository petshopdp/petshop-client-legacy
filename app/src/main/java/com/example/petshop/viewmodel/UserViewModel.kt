package com.example.petshop.viewmodel

import android.app.AlertDialog
import android.view.View
import androidx.databinding.ObservableBoolean
import androidx.databinding.ObservableInt
import com.example.petshop.net.data.User
import com.example.petshop.other.validation
import com.example.petshop.ui.auth.AuthActivity
import com.google.android.material.snackbar.Snackbar
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.apache.commons.codec.digest.DigestUtils

class UserViewModel : BaseViewModel(){

    val RESPONSE_GOOD = 1
    val RESPONS_BAD = 2
    val user = User.activeUser
    var visibilityPassword = ObservableInt(View.INVISIBLE)
    var enabledPersonEt = ObservableBoolean(false)
    var oldPassword = ""
    var newPassword = ""
    var repeatPassword = ""

    override fun create() {
        super.create()
        AuthActivity.appComponent.inject(this)
    }

    fun setVisibility() {
        if (visibilityPassword.get() == View.VISIBLE)
            visibilityPassword.set(View.INVISIBLE)
        else
            visibilityPassword.set(View.VISIBLE)
    }

    fun setEnabled() {
        if (!enabledPersonEt.get())
            enabledPersonEt.set(true)
        else
            enabledPersonEt.set(false)
    }

    fun savePassword(v: View) {
        if (newPassword == repeatPassword)
            if (validation.matches(newPassword))
                compositeDisposable.add(
                    userRep.changePassword(User.activeUser.id, DigestUtils.md5Hex(oldPassword),DigestUtils.md5Hex(newPassword))
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe {
                            if (it.message.id == RESPONSE_GOOD) {
                                User.activeUser.password = newPassword
                            }
                            Snackbar.make(v, it.message.text, Snackbar.LENGTH_SHORT).show()
                        }
                )
            else
                AlertDialog.Builder(v.context)
                    .setTitle("Внимание!")
                    .setMessage("Пароль в пароле должна быть минимум одна строчная буква, одна заглавная, одна цифра и длинна пароля не менее шести символов")
                    .setPositiveButton("OK") { _, _ ->  }
                    .create().show()
        else
            Snackbar.make(v, "Пароли не совпадают", Snackbar.LENGTH_SHORT).show()
    }

    fun saveUserData(v: View) {
        compositeDisposable.add(
            userRep.changeUserData(user.id, user.email, user.name, user.surname)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    if (it.message.id == RESPONSE_GOOD) {
                        User.activeUser = user
                    }
                    Snackbar.make(v, it.message.text, Snackbar.LENGTH_SHORT).show()
                }
        )
    }

}