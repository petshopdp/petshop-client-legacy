package com.example.petshop.viewmodel

import android.view.View
import android.widget.SeekBar
import androidx.databinding.ObservableField
import androidx.databinding.ObservableInt
import com.example.petshop.R
import com.example.petshop.adapters.rv.GoodsAdapter
import com.example.petshop.helper.AddFilterHelper
import com.example.petshop.net.data.AnimalType
import com.example.petshop.net.data.Brand
import com.example.petshop.net.data.RawGood
import com.example.petshop.net.data.Type
import com.example.petshop.net.repository.GoodsRepository
import com.example.petshop.other.LINEAR
import com.example.petshop.ui.auth.AuthActivity
import com.example.petshop.ui.base.BaseActivity
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class GoodsViewModel : BaseViewModel(){

    private val filteredGoods = ArrayList<RawGood>()
    private var filteredBrand: Brand? = null
    private var filteredType: Type? = null
    private var filteredAnimalType: AnimalType? = null
    val rep = GoodsRepository()
    val adapter = ObservableField<GoodsAdapter>()
    val price = ObservableInt()
    val max = ObservableInt()
    var helper: AddFilterHelper? = null
    var isChecked = false

    override fun create() {
        super.create()
        AuthActivity.appComponent.inject(this)
    }

    fun load() {
        compositeDisposable.add(rep.getGoods()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe{
                adapter.set(GoodsAdapter(it.goods))
                helper?.setPrice(it.goods)
            })
    }

    fun loadBrands() {
        compositeDisposable.add(
            rep.getBrands()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    helper?.setBrands(it.brands)
                })
    }

    fun loadAnimalTypes() {
        compositeDisposable.add(
            rep.getAnimalTypes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    helper?.setAnimalTypes(it.types)
                })
    }

    fun loadTypes() {
        compositeDisposable.add(
            rep.getTypes()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe{
                    helper?.setTypes(it.types)
                })
    }

    fun changePrice(pos: Int) {
        price.set(pos)
    }

    fun changedPrice(seekbar: SeekBar) {
        filteredGoods.clear()
        loadValidGoods()
        adapter.set(GoodsAdapter(filteredGoods))
    }

    fun checkboxChange(isChecked: Boolean) {
        this.isChecked = isChecked
        filteredGoods.clear()
        loadValidGoods()
        adapter.set(GoodsAdapter(filteredGoods))
    }


    fun filterGoodsList(pos: Int, v: View) {
        if (pos != 0) {
            setFilterParameter(v, pos)
            if (filteredGoods.isEmpty()) {
                loadValidGoods()
            } else {
                var i = 0
                while (i < filteredGoods.size) {
                    if (!isValid(filteredGoods[i]))
                        filteredGoods.removeAt(i)
                    else i++
                }
                loadValidGoods()
            }
            adapter.set(GoodsAdapter(filteredGoods))
        } else {
            unsetFilterParameter(v)
            if (isNotFiltered())
                adapter.set(GoodsAdapter(rep.goods))
            else {
                loadValidGoods()
                adapter.set(GoodsAdapter(filteredGoods))
            }

        }

    }


    private fun loadValidGoods() {
        for (i in rep.goods)
            if (isValid(i)) filteredGoods.add(i)
    }

    private fun isValid(good: RawGood) : Boolean{
        if (!((good.brand == filteredBrand || filteredBrand == null) && !filteredGoods.contains(good)))
            return false
        if (!(good.type == filteredType || filteredType == null && !filteredGoods.contains(good)))
            return false
        if (!(good.animalType == filteredAnimalType || filteredAnimalType == null && !filteredGoods.contains(good)))
            return false
        if (good.price < price.get())
            return false
        if(isChecked && good.count == 0)
            return false
        return true
    }

    private fun isNotFiltered() = filteredGoods.isEmpty() && filteredBrand == null && filteredType == null
            && filteredAnimalType == null

    private fun setFilterParameter(v: View, pos: Int) {
        when (v.id) {
            R.id.s_brand -> filteredBrand = rep.brands.get(pos)
            R.id.s_type -> filteredType = rep.types.get(pos)
            R.id.s_animal_type -> filteredAnimalType = rep.animalTypes.get(pos)
        }
    }

    private fun unsetFilterParameter(v: View) {
        when (v.id) {
            R.id.s_brand -> filteredBrand = null
            R.id.s_type -> filteredType = null
            R.id.s_animal_type -> filteredAnimalType = null
        }
    }
    companion object {
        @JvmStatic
        val MANAGER = LINEAR
        var categoryId = 0
    }
}