package com.example.petshop.viewmodel

import androidx.lifecycle.ViewModel
import com.example.petshop.net.repository.UserRepository
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

abstract class BaseViewModel : ViewModel() {

    @Inject
    lateinit var userRep: UserRepository

    lateinit var compositeDisposable: CompositeDisposable
    open fun create() {
        compositeDisposable = CompositeDisposable()
    }



}