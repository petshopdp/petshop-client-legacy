package com.example.petshop.adapters.rv

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.Basket
import com.example.petshop.viewmodel.BasketViewModel

class BasketAdapter(val basketItems: ArrayList<Basket>, private val vm: BasketViewModel) : RecyclerView.Adapter<BasketAdapter.VH>() {

    class VH(val item: ViewDataBinding) : RecyclerView.ViewHolder(item.root) {
        fun bind(basketItem: Basket) {
            item.setVariable(BR.item, basketItem)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), R.layout.item_basket, parent,false)
        return VH(binding)
    }

    override fun getItemCount(): Int {
        return basketItems.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(basketItems[position])
        holder.item.root.findViewById<ImageView>(R.id.btn_delete).setOnClickListener {
            deleteItem(holder.itemView, position)
        }

    }

    fun refresh(pos: Int) {
        notifyItemRemoved(pos)
        notifyItemRangeChanged(pos, basketItems.size)
    }

    private fun deleteItem(v: View, pos: Int) {
        vm.removeItems(v,basketItems[pos], pos)

    }


}