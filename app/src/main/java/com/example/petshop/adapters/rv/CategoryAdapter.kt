package com.example.petshop.adapters.rv

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.Category
import com.example.petshop.ui.goods.GoodsActivity

class CategoryAdapter(private val categories : List<Category>) : RecyclerView.Adapter<CategoryAdapter.VH>() {

    class VH(val item: ViewDataBinding) : RecyclerView.ViewHolder(item.root) {
        fun bind(category: Category) {
            item.setVariable(BR.category, category)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val inflater = LayoutInflater.from(parent.context)
        val binding = DataBindingUtil.inflate<ViewDataBinding>(inflater, R.layout.item_category, parent, false)
        return VH(binding)
    }

    override fun getItemCount(): Int {
        return categories.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(categories[position])
        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, GoodsActivity::class.java)
            intent.putExtra(GoodsActivity.CATEGORY_ID, categories[position].id)
            holder.itemView.context.startActivity(intent)
        }
    }
}