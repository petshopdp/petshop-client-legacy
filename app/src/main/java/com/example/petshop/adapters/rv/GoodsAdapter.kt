package com.example.petshop.adapters.rv

import android.content.Intent
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.recyclerview.widget.RecyclerView
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.RawGood
import com.example.petshop.ui.detail.DetailActivity
import com.example.petshop.ui.goods.GoodsActivity
import com.example.petshop.viewmodel.GoodsViewModel

class GoodsAdapter(private val goods: List<RawGood>) : RecyclerView.Adapter<GoodsAdapter.VH>() {

    class VH(val item: ViewDataBinding) : RecyclerView.ViewHolder(item.root) {
        fun bind(good: RawGood) {
            item.setVariable(BR.good, good)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
        val binding = DataBindingUtil.inflate<ViewDataBinding>(
            LayoutInflater.from(parent.context), R.layout.item_good, parent,false)
        return VH(binding)
    }

    override fun getItemCount(): Int {
        return goods.size
    }

    override fun onBindViewHolder(holder: VH, position: Int) {
        holder.bind(goods[position])
        holder.itemView.setOnClickListener{
            val intent = Intent(holder.itemView.context, DetailActivity::class.java)
            intent.putExtra(DetailActivity.ITEM_ID, goods[position].id)
            intent.putExtra(GoodsActivity.CATEGORY_ID, GoodsViewModel.categoryId)
            holder.itemView.context.startActivity(intent)
        }
    }
}