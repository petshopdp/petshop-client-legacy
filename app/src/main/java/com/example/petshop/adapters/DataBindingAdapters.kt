package com.example.petshop.adapters

import android.app.Activity
import android.net.Uri
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.petshop.other.GRID
import com.example.petshop.other.LINEAR
import com.github.twocoffeesoneteam.glidetovectoryou.GlideToVectorYou
import com.google.android.material.navigation.NavigationView

@BindingAdapter("app:url")
fun loadSVG(v: ImageView, uri: String?) {
    if (uri != null) GlideToVectorYou.justLoadImage(v.context as Activity,Uri.parse(uri), v)
}
@BindingAdapter("app:img")
fun loadImage(v: ImageView, uri: String?) {
    if (uri != null) Glide.with(v.context).load(uri).into(v)
}

@BindingAdapter("menuClickListener")
fun menuClickListener(v: NavigationView, listener: NavigationView.OnNavigationItemSelectedListener) {
    v.setNavigationItemSelectedListener(listener)
}

@BindingAdapter("layoutManager")
fun setLayoutManager(rv: RecyclerView, type: String) {
    when (type) {
        LINEAR -> rv.layoutManager = LinearLayoutManager(rv.context)
        GRID -> rv.layoutManager = GridLayoutManager(rv.context, 2)
    }
}