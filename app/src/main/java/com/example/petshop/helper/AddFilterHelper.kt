package com.example.petshop.helper

import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.AnimalType
import com.example.petshop.net.data.Brand
import com.example.petshop.net.data.RawGood
import com.example.petshop.net.data.Type
import com.example.petshop.other.*
import com.example.petshop.viewmodel.GoodsViewModel

class AddFilterHelper(val container : FrameLayout, val vm: GoodsViewModel) {

    var item: ViewDataBinding? = null

    fun initAppbar(category: Int) {
        when(category) {
            FOOD -> addAppbarWithAnimalTypes()
            CARE -> addAppbarWithAnimalTypes()
            CAGE -> addDefaultAppbar()
            TOYS -> addAppbarWithAnimalTypes()
            FILLERS -> addAppbarWithAnimalTypes()
            ANIMALS -> addAnimalAppbar()
        }
    }

    private fun addAnimalAppbar() {
        item = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.appbar_animal, container, false)
        item?.setVariable(BR.vm, vm)
        container.addView(item?.root)
        vm.loadTypes()

    }

    private fun addAppbarWithAnimalTypes() {
        item = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.appbar_food, container, false)
        item?.setVariable(BR.vm, vm)
        container.addView(item?.root)
        vm.loadBrands()
        vm.loadAnimalTypes()
        vm.loadTypes()

    }

    private fun addDefaultAppbar() {
        item = DataBindingUtil.inflate(LayoutInflater.from(container.context), R.layout.appbar_default, container, false)
        item?.setVariable(BR.vm, vm)
        container.addView(item?.root)
        vm.loadBrands()
        vm.loadTypes()
    }

    fun setBrands(brands: ArrayList<Brand>) {
        item?.setVariable(BR.brands, brands)
    }

    fun setAnimalTypes(types: ArrayList<AnimalType>) {
        item?.setVariable(BR.aTypes, types)
    }

    fun setTypes(types: ArrayList<Type>) {
        item?.setVariable(BR.types, types)
    }


    fun setPrice(goods: ArrayList<RawGood>) {
        var max = goods[0].price
        for(i in goods) {
            if (i.price > max) max = i.price
        }
        vm.max.set(max)
    }

}