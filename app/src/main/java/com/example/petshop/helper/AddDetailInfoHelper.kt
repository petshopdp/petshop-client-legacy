package com.example.petshop.helper

import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.example.petshop.BR
import com.example.petshop.R
import com.example.petshop.net.data.RawGood
import com.example.petshop.other.*

class AddDetailInfoHelper(val linearLayout: LinearLayout) {

    var good: RawGood? = null

    fun setItem(item: RawGood) {
        good = item
        addMainItem()
        when(item.categoryId) {
            FOOD -> addFoodItem()
            TOYS -> addDefaultItem()
            CARE -> addDefaultItem()
            CAGE -> addCageItem()
            FILLERS -> addDefaultItem()
            ANIMALS -> addAnimalsItem()
        }
        addInfoItem()
    }

    //Я вот думаю. А что если всё нижеперечисленное сделать одним методом и в качестве параметра передавать ему определённый layout?
    //Они-то по сути только им и отличаются
    private fun addMainItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_main, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.good, good)
    }
    private fun addFoodItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_food, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.food, good)
    }

    private fun addCageItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_cage, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.cage, good)
    }

    private fun addDefaultItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_default, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.good, good)
    }

    private fun addAnimalsItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_animal, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.animal, good)
    }

    private fun addInfoItem() {
        val item = DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(linearLayout.context), R.layout.card_sale_info, linearLayout, false)
        linearLayout.addView(item.root)
        item.setVariable(BR.good, good)
    }
}